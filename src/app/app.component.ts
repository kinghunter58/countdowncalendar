import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  days;
  
  vk = new Date(2018,11,22);
  ngOnInit(): void {
    let today = new Date();
    let timeDiff = Math.abs(this.vk.getTime() - today.getTime());
    let diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    this.days = diffDays;
  }
}
